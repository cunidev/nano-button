# nano-button

A pure HTML / CSS button to accept NANO (RaiBlocks) donations on your website, inspired by the nano.org design guidelines to look more official. Not affiliated with the NANO foundation.

## Demo

![nano-button in action](screenshot.gif)

## Usage

Just copy the `nano-popover` section of the demo HTML file into your website to embed the widget. Remember to change *both* the QR code image (`images/nano-wallet.png`) and addresses to those of your Nano wallet before implementing the widget!

## License

Unlimited usage for free non-commercial usage. Please [open an issue](https://gitlab.com/cunidev/nano-button/issues) for commercial usage. Credit comments should be left in source code, and a link to the [project page](https://gitlab.com/cunidev/nano-button) in your impressum or footer is not mandatory, but still welcome, and will help other people find this as well.

## Tips

You can tip here: `nano_16x9cqkb1b375cc78ihuiy3npwac1smk6qpid18hhfrt55swsyj1dkw1z9i6`
